%{
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern int exitcode;

#define YYSTYPE char*

void yyerror(const char *s);
extern char *yytext;
extern FILE *yyin;

enum SymbolOcc{ SYMBOL_ANY = 0, SYMBOL_USE = 1, SYMBOL_DEF = 2 };
enum SymbolType{ SYMBOL_LABEL = 1, SYMBOL_VARIABLE = 2 };

struct StrList{
	char *s;
	enum SymbolType typ;
	enum SymbolOcc occ;
	struct StrList *next;
};

struct StrList *newStrList(char *word, enum SymbolOcc occ, enum SymbolType typ, struct StrList *next)
{
	struct StrList *lst = malloc(sizeof(struct StrList));
	lst->s = word;
	lst->typ = typ;
	lst->occ = occ;
	lst->next = next;
	return lst;
}

struct StrList *concatStrList(struct StrList *x, struct StrList *y)
{
	struct StrList **z = &x;
	while(*z != NULL){
		z = &((*z)->next);
	}
	*z = y;
	return x;
}

void printStrList(const struct StrList *x)
{
	if(x == NULL){
		printf("StrList: (empty)\n");
		return;
	}
	printf("StrList:\n");
	do{
		printf(" - %s \t %s %s\n", x->s, 
			x->occ == SYMBOL_USE ? "USE" : "DEF",
			x->typ == SYMBOL_LABEL ? "LABEL" : "VARIABLE");
		x = x->next;
	}while(x != NULL);
}

struct StrList* existsStrEx(struct StrList *x, const char *w, int occ, int typ)
{
	assert(w != NULL);
	if(x != NULL){
		printf("looking for %s %d %d ...\n", w, occ, typ);
		do{
			printf("   %s %d %d \n", x->s, x->occ, x->typ);
			if(((x->occ == occ && occ != SYMBOL_ANY) || occ == SYMBOL_ANY) &&
			   ((x->typ == typ && typ != SYMBOL_ANY) || typ == SYMBOL_ANY) != 0 && strcmp(x->s, w) == 0){
				break;
			}
			x = x->next;
		}while(x != NULL);
	}
	return x;
}
struct StrList* existsStr(struct StrList *x, const char *w)
{
	return existsStrEx(x, w, SYMBOL_ANY, SYMBOL_ANY);
}

/* return 0 -> there were errors */
int checkStrList(struct StrList *x)
{
	struct StrList *top = x;
	while(x != NULL){
		const struct StrList *y;
		switch(x->typ){
		case SYMBOL_VARIABLE:
			if(x->occ == SYMBOL_DEF && (y = existsStr(x->next, x->s)) != NULL){
				if(y->occ == SYMBOL_DEF){
					fprintf(stderr,"error: variable %s is defined multiply\n", x->s);
				}else{
					fprintf(stderr,"error: variable %s used before definition\n", x->s);
				}
				return 0;
			}else if(x->occ == SYMBOL_USE && existsStrEx(x->next, x->s, SYMBOL_DEF, SYMBOL_VARIABLE) == NULL){
				fprintf(stderr, "error: variable %s used but not defined\n", x->s);
				return 0;
			}
			break;
		case SYMBOL_LABEL:
			if(x->occ == SYMBOL_DEF && (y = existsStrEx(x->next, x->s, SYMBOL_DEF, SYMBOL_ANY)) != NULL){
				if(y->typ == SYMBOL_LABEL){
					fprintf(stderr,"error: label %s is defined multiply\n", x->s);
				}else if(y->typ == SYMBOL_VARIABLE){
					fprintf(stderr,"error: variable %s cannot be used as jump target\n", x->s);
				}
				return 0;
			}else if(x->occ == SYMBOL_USE && existsStrEx(top, x->s, SYMBOL_DEF, SYMBOL_LABEL) == NULL){
				fprintf(stderr, "error: label %s used but not defined\n", x->s);
				return 0;
			}
			break;
		default:
			assert(0);
			break;
		}
		x = x->next;
	}
	return 1;
}

%}

%token KW_IF
%token KW_AND
%token KW_END
%token KW_NOT
%token KW_VAR
%token KW_GOTO
%token KW_RETURN
%token OP_NE
%token ID 
%token NUM

/* @attributes { struct ST *st; } ParList */
@attributes { struct StrList *lst; } ParList Funcdef Labeldefs Stats Stat Cond CondAnd Cterm Expr PlusTerms MulTerms PreMinusTerm ExprList NEExprList Term
@attributes { char *str; } ID
@traversal @lefttoright @postorder post 

%%

Start:		Program;
Program:	
		| Funcdefs
		;
Funcdefs:	Funcdef ';'
		@{	
			@post printStrList(@Funcdef.lst@);
			@post if(checkStrList(@Funcdef.lst@) == 0) exitcode=3;
		@}
		| Funcdefs Funcdef ';'
		@{	
			@post printStrList(@Funcdef.lst@);
			@post if(checkStrList(@Funcdef.lst@) == 0) exitcode=3;
		@}
		;
Funcdef:	ID '(' ParList ')' Stats KW_END /* Funktionsdefinition */
		@{	@e Funcdef.lst : ParList.lst Stats.lst ;
			   @Funcdef.lst@ = concatStrList(@Stats.lst@, @ParList.lst@);
		@}
		| ID '(' ')' Stats KW_END
		@{	@e Funcdef.lst : Stats.lst ;
			   @Funcdef.lst@ = @Stats.lst@;
		@}
		| ID '(' ParList ')' KW_END
		@{	@e Funcdef.lst : ParList.lst ;
			   @Funcdef.lst@ = @ParList.lst@;
		@}
		| ID '(' ')' KW_END
		@{	@e Funcdef.lst : ;
			   @Funcdef.lst@ = NULL;
		@}
		;
ParList:	ParList ',' ID
		@{	@e ParList.lst : ParList.1.lst ID.str;
			   @ParList.lst@ = newStrList(@ID.str@, SYMBOL_DEF, SYMBOL_VARIABLE, @ParList.1.lst@);
		@}
		| ParList ',' 
		@{	@e ParList.lst : ParList.1.lst;
			   @ParList.lst@ = @ParList.1.lst@;
		@}
			
		| ID
		@{	@e ParList.lst : ID.str;
			   @ParList.lst@ = newStrList(@ID.str@, SYMBOL_DEF, SYMBOL_VARIABLE, NULL);
		@}
		;
Stats:		Labeldefs Stat ';'
		@{	@e Stats.lst : Labeldefs.lst Stat.lst ;
			   @Stats.lst@ = concatStrList(@Stat.lst@, @Labeldefs.lst@) ;
		@}
		| Stats Labeldefs Stat ';'
		@{	@e Stats.lst : Stats.1.lst Labeldefs.lst Stat.lst ;
			   @Stats.lst@ = concatStrList(@Stat.lst@, concatStrList(@Labeldefs.lst@, @Stats.1.lst@)) ;
		@}
		;
Labeldefs:	
		@{	@e Labeldefs.lst : ;
			   @Labeldefs.lst@ = NULL;
		@}
		| Labeldefs ID ':'
		@{	@e Labeldefs.lst : Labeldefs.1.lst ID.str;
			   @Labeldefs.lst@ = newStrList(@ID.str@, SYMBOL_DEF, SYMBOL_LABEL, @Labeldefs.1.lst@);
		@}
		;

Stat:		KW_RETURN Expr
		@{	@e Stat.lst : Expr.lst ;
			   @Stat.lst@ = @Expr.lst@ ;
		@}
		| KW_GOTO ID
		@{	@e Stat.lst : ID.str ;
			   @Stat.lst@ = newStrList(@ID.str@, SYMBOL_USE, SYMBOL_LABEL, NULL) ;
		@}
		| KW_IF Cond KW_GOTO ID 
		@{	@e Stat.lst : Cond.lst ID.str ;
			   @Stat.lst@ = concatStrList(newStrList(@ID.str@, SYMBOL_USE, SYMBOL_LABEL, NULL), @Cond.lst@) ;
		@}
		| KW_VAR ID '=' Expr /* Variablendefinition */
		@{	@e Stat.lst : ID.str Expr.lst ;
			   @Stat.lst@ = concatStrList(newStrList(@ID.str@, SYMBOL_DEF, SYMBOL_VARIABLE, NULL), @Expr.lst@); ;
			   /* actually @Expr.lst@ should come first here in concat, because it comes after ID
			      but this would define the symbol before Expr so Expr could contain the ID */
		@}
		| Term '[' Expr ']' '=' Expr /* schreibender Arrayzugriff */
		@{	@e Stat.lst : Term.lst Expr.0.lst Expr.1.lst ;
			   @Stat.lst@ = concatStrList(@Expr.1.lst@, @Expr.0.lst@) ;
		@}
		| ID '=' Expr /* schreibender Variablenzugriff */
		@{	@e Stat.lst : ID.str Expr.lst ;
			   @Stat.lst@ = concatStrList(@Expr.lst@, newStrList(@ID.str@, SYMBOL_USE, SYMBOL_VARIABLE, NULL)) ;
		@}
		| Term
		@{	@e Stat.lst : Term.lst ;
			   @Stat.lst@ = @Term.lst@ ;
		@}
		;
Cond:		CondAnd
		@{	@i @Cond.lst@ = @CondAnd.lst@ ; @}
		| KW_NOT Cterm
		@{	@i @Cond.lst@ = @Cterm.lst@ ; @}
		;
CondAnd:	Cterm
		@{	@i @CondAnd.lst@ = @Cterm.lst@ ; @}
		| CondAnd KW_AND Cterm
		@{	@i @CondAnd.lst@ = concatStrList(@Cterm.lst@, @CondAnd.1.lst@) ; @}
		;
Cterm:		'(' Cond ')'
		@{	@i @Cterm.lst@ = @Cond.lst@ ; @}
		| Expr OP_NE Expr
		@{	@i @Cterm.lst@ = concatStrList(@Expr.1.lst@, @Expr.0.lst@) ; @}
		| Expr '>' Expr
		@{	@i @Cterm.lst@ = concatStrList(@Expr.1.lst@, @Expr.0.lst@) ; @}
		;
Expr:		PlusTerms
		@{	@i @Expr.lst@ = @PlusTerms.lst@ ; @}
		| MulTerms
		@{	@i @Expr.lst@ = @MulTerms.lst@ ; @}
		| PreMinusTerm 
		@{	@i @Expr.lst@ = @PreMinusTerm.lst@ ; @}
		;
PlusTerms:	Term '+' Term
		@{	@i @PlusTerms.lst@ = concatStrList(@Term.1.lst@, @Term.0.lst@) ; @}
		| PlusTerms '+' Term
		@{	@i @PlusTerms.lst@ = concatStrList(@Term.lst@, @PlusTerms.1.lst@) ; @}
		;
MulTerms:	Term '*' Term
		@{	@i @MulTerms.lst@ = concatStrList(@Term.1.lst@, @Term.0.lst@) ; @}
		| MulTerms '*' Term
		@{	@i @MulTerms.lst@ = concatStrList(@Term.lst@, @MulTerms.1.lst@) ; @}
		;
PreMinusTerm:	Term
		@{	@i @PreMinusTerm.lst@ = @Term.lst@ ; @}
		| '-' PreMinusTerm 
		@{	@i @PreMinusTerm.0.lst@ = @PreMinusTerm.1.lst@ ; @}
		;
ExprList:	@{	@i @ExprList.lst@ = NULL ; @}
		| NEExprList
		@{	@i @ExprList.lst@ = @NEExprList.lst@ ; @}
		;
NEExprList:	NEExprList ',' Expr
		@{	@i @NEExprList.0.lst@ = concatStrList(@Expr.lst@, @NEExprList.1.lst@) ; @}
		| NEExprList ',' 
		@{	@i @NEExprList.0.lst@ = @NEExprList.1.lst@ ; @}
		| Expr 
		@{	@i @NEExprList.lst@ = @Expr.lst@ ; @}
		;
Term:		'(' Expr ')'
		@{	@i @Term.lst@ = @Expr.lst@ ; @}
		| NUM
		@{	@i @Term.lst@ = NULL; @}
		| Term '[' Expr ']' /* lesender Arrayzugriff */
		@{	@i @Term.0.lst@ = concatStrList(@Expr.lst@, @Term.1.lst@); @}
		| ID /* lesender Variablenzugriff */
		@{	@i @Term.lst@ = newStrList(@ID.str@, SYMBOL_USE, SYMBOL_VARIABLE, NULL) ; @}
		| ID '(' ExprList ')' /* Funktionsaufruf */
		@{	@i @Term.0.lst@ = @ExprList.lst@ ;
			/* this ID is a function !!! don't use! */
		@}
		;
%%

void yyerror(const char *s)
{
	fprintf(stderr, "error: %s\n", s);
	exit(2);
}

int main()
{
	yyin = stdin;
	yyparse();
	return exitcode;
}

