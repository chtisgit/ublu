%top{
#include "oxout.tab.h"

int exitcode = 0;

}

%x comment

ONE	";"|"("|")"|","|":"|"="|">"|"["|"]"|"-"|"+"|"*"
TWO	"!="
ID	[A-Za-z][A-Za-z0-9]*
NUM	[0-9_]+
COMM	"(""*"[^*]*"*"")"

%%

<INITIAL>"(""*"		BEGIN(comment);
<comment>"*"")"		BEGIN(INITIAL);
<comment>[^*]*		/* eat */
<comment>"*"[^*)]*	/* eat */

#{COMM}	;
{ONE}	return yytext[0];
"!="	return OP_NE;
if	return KW_IF;
and	return KW_AND;
end	return KW_END;
not	return KW_NOT;
var	return KW_VAR;
goto	return KW_GOTO;
return	return KW_RETURN;

{ID}	return ID;	@{
				@ID.str@ = strdup(yytext);
			@}
{NUM}	{
		int i, j = 0;
		for(i = 0; yytext[i] != '\0'; i++){
			if(yytext[i] != '_'){
				yytext[j++] = yytext[i];
			}
		}
		yytext[j] = '\0';
		printf("num %d\n", yytext);
		return NUM;
	} /* @{ @NUM.value@ = atol(yytext */
[ \t\n]	/* eat */
.	{
		printf("error\n");
		exit(1);
	}


%%

int yywrap()
{
	return 1;
}
