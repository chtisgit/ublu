	.file	"asma.c"
	.text
.globl asma
	.type	asma, @function
asma:
.LFB0:
	.cfi_startproc

	# number 1 ... a_1 a_2 = a_1 * 2^64 + a_2

	# rsi = a_1
	# rdi = a_2
	# rdx = b
	# rcx = addr

	push %rbp
	mov %rsp, %rbp
	sub $16, %rsp

	movq %rbx, -8(%rbp)

	mov %rcx, %rbx
	mov %rdx, %rcx

	# rcx = b
	# rbx = addr

	mov %rdi, %rax
	mul %rcx
	mov %rax, (%rbx)
	mov %rdx, -16(%rbp)

	mov %rsi, %rax
	xor %rsi, %rsi
	mul %rcx
	add -16(%rbp), %rax
	mov %rax, 8(%rbx)
	adc %rsi, %rdx
	mov %rdx, 16(%rbx)
	
	mov -8(%rbp), %rbx
	leave
	ret
	.cfi_endproc

.globl asma32
	.type	asma32, @function
.LFE0:
	.size	asma, .-asma
	.ident	"GCC: (Debian 4.4.5-8) 4.4.5"
	.section	.note.GNU-stack,"",@progbits
