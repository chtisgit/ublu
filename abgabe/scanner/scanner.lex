%top{

int exitcode = 0;

}

%x comment

ONE	";"|"("|")"|","|":"|"="|">"|"["|"]"|"-"|"+"|"*"
TWO	"!="
ID	[A-Za-z][A-Za-z0-9]*
NUM	[0-9_]+
COMM	"(""*"[^*]*"*"")"

%%

<INITIAL>"(""*"		BEGIN(comment);
<comment>"*"")"		BEGIN(INITIAL);
<comment>[^*]*		/* eat */
<comment>"*"[^*)]*	/* eat */

{ONE}	printf("%s\n",yytext);
{TWO}	printf("%s\n",yytext);
if	printf("%s\n",yytext);
and	printf("%s\n",yytext);
end	printf("%s\n",yytext);
not	printf("%s\n",yytext);
var	printf("%s\n",yytext);
goto	printf("%s\n",yytext);
return	printf("%s\n",yytext);

{ID}	printf("id %s\n",yytext);
{NUM}	{
		const char *s = yytext;
		while(*s == '0' || *s == '_'){
			s++;
		}
		if(*s == '\0'){
			s = "0";
		}
		printf("num ");
		while(*s != '\0'){
			if(*s != '_'){
				printf("%c", *s);
			}
			s++;
		}
		printf("\n");
	}
[ \t\n]	/* eat */
.	{
		printf("error\n");
		exitcode = 1;
	}


%%

int yywrap()
{
	return 1;
}

int main(int argc, char **argv)
{
	yyin = stdin;
	yylex();
	return exitcode;
}
