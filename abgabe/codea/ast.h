#pragma once

#include <stdlib.h>
#include <stdint.h>

enum ASTT{
	ASTT_STATEMENT=0,
	ASTT_GOTO=1,
	ASTT_RETURN=2,
	ASTT_VARREAD=3,
	ASTT_VARWRITE=4,
	ASTT_PLUS=5,
	ASTT_MINUS=6,
	ASTT_MULT=7,
	ASTT_ARRAYREAD=8,
	ASTT_ARRAYWRITE=9,
	ASTT_INT=10,
	ASTT_LEA=11
};

struct AST{
	enum ASTT type;
	struct AST *left, *right;
	const char *n;
	const char *reg;
	int64_t imm;
	struct burm_state *label;
};
#define TREENULL		((struct AST *)NULL)
#define TREESIZE		(sizeof(struct AST))
#define TREECAST		struct AST*
#define NODEPTR_TYPE		struct AST*
#define OP_LABEL(node)		((node)->type)
#define LEFT_CHILD(node)	((node)->left)
#define RIGHT_CHILD(node)	((node)->right)
#define STATE_LABEL(node)	((node)->label)
#define PANIC(...)		fprintf(stderr, __VA_ARGS__)

struct AST* ast_new(enum ASTT type, struct AST *left, struct AST *right);
struct AST* ast_return(struct AST *left);
struct AST* ast_goto(const char *n);
struct AST* ast_plus(struct AST *left, struct AST *right);
struct AST* ast_mult(struct AST *left, struct AST *right);
struct AST* ast_minus(struct AST *left);
struct AST* ast_int(int64_t x);
struct AST* ast_varread(const char *s);
struct AST* ast_varwrite(const char *n, struct AST *left);
struct AST* ast_arraywrite(struct AST *left, struct AST *right);
struct AST* ast_arrayread(struct AST* left);
struct AST* ast_lea(struct AST* left, struct AST* right);
