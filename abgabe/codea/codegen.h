#pragma once

#include "ast.h"
#include "strlist.h"
#include "regalloc.h"
#include "burm.c"

extern int exitcode;

struct RegAllocator *regallocator = NULL;

inline void func_header(const char *name)
{
	printf(".globl %s\n.type %s, @function\n%s:\n", name, name, name);
}

inline void func_footer(const char *name)
{
	printf("ret # end function %s\n", name);
}


void reserve_vars(struct RegAllocator *a, struct StrList *lst, struct AST *ast, int p_count)
{
	int p = 0, remaining;
	struct StrList *x, *xx, *last = NULL;
	
	for(;;){
		const char *reg;

		remaining = 0;
		for(x = lst; x != last; x = x->next){
			if(x->occ == SYMBOL_DEF && x->typ == SYMBOL_VARIABLE){
				xx = x;
				remaining++;
			}
		}
		if(remaining == 0) break;

		last = xx;
		if(p < p_count){
			reg = regalloc_parameter(a, xx->s);
			p++;
			//fprintf(stderr, "registered parameter %s\n", xx->s);
		}else{
			reg = regalloc_var(a, xx->s);
			//fprintf(stderr, "registered variable %s\n", xx->s);
		}

	}

	assert(p == p_count);
}

void indent(int n)
{
	while(--n >= 0){
		fputc(' ',stderr);
	}
}

void traverse_tree(struct AST* ast, int n)
{
	if(ast == NULL) return;
	indent(n);
	fprintf(stderr, "+ %d\n", ast->type);
	traverse_tree(ast->left, n+1);
	traverse_tree(ast->right, n+1);
}

void function(const char *name, struct StrList *lst, struct AST *ast, int p_count)
{
	struct RegAllocator a = regalloc_new();
	regallocator = &a;
	if(checkStrList(lst) == 0){
		exitcode = 3;
		return;
	}

	reserve_vars(&a, lst, ast, p_count);
	func_header(name);

	//traverse_tree(ast, 0);

	if(ast != NULL){
		burm_label(ast);
		burm_reduce(ast, 1);
	}
	if(ast == NULL || (ast->type != ASTT_RETURN && 
		!(ast->type == ASTT_STATEMENT && ast->right->type == ASTT_RETURN))){

		func_footer(name);
	}

	regallocator = NULL;
}
