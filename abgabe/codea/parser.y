%{
#include "strlist.h"
#include "codegen.h"
#include "ast.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern int exitcode;

#define YYSTYPE char*

void yyerror(const char *s);
extern char *yytext;
extern FILE *yyin;

%}

%token KW_IF
%token KW_AND
%token KW_END
%token KW_NOT
%token KW_VAR
%token KW_GOTO
%token KW_RETURN
%token OP_NE
%token ID 
%token NUM

/* @attributes { struct ST *st; } ParList */
@attributes { struct StrList *lst; } Labeldefs Cond CondAnd Cterm ExprList NEExprList 
@attributes { struct StrList *lst; int p_count; } ParList
@attributes { struct StrList *lst; struct AST *ast; } Stats Stat Expr PlusTerms MulTerms PreMinusTerm Term
@attributes { struct StrList *lst; struct AST *ast; char *name; int p_count; } Funcdef
@attributes { char *str; } ID
@attributes { long value; } NUM
@traversal @lefttoright @preorder pre
@traversal @lefttoright @postorder post 

%%

Start:		Program;
Program:	
		| Funcdefs
		;
Funcdefs:	Funcdef ';'
		@{
			@post function(@Funcdef.name@, @Funcdef.lst@, @Funcdef.ast@, @Funcdef.p_count@);
		@}
		| Funcdefs Funcdef ';'
		@{	
			@post function(@Funcdef.name@, @Funcdef.lst@, @Funcdef.ast@, @Funcdef.p_count@);
		@}
		;
Funcdef:	ID '(' ParList ')' Stats KW_END /* Funktionsdefinition */
		@{	@i @Funcdef.lst@ = concatStrList(@Stats.lst@, @ParList.lst@);
			@i @Funcdef.name@ = @ID.0.str@;
			@i @Funcdef.ast@ = @Stats.ast@;
			@i @Funcdef.p_count@ = @ParList.p_count@;
		@}
		| ID '(' ')' Stats KW_END
		@{	@i @Funcdef.lst@ = @Stats.lst@;
			@i @Funcdef.name@ = @ID.0.str@;
			@i @Funcdef.ast@ = @Stats.ast@;
			@i @Funcdef.p_count@ = 0;
		@}
		| ID '(' ParList ')' KW_END
		@{	@i @Funcdef.lst@ = @ParList.lst@;
			@i @Funcdef.name@ = @ID.0.str@;
			@i @Funcdef.ast@ = NULL;
			@i @Funcdef.p_count@ = @ParList.p_count@;
		@}
		| ID '(' ')' KW_END
		@{	@i @Funcdef.lst@ = NULL;
			@i @Funcdef.name@ = @ID.0.str@;
			@i @Funcdef.ast@ = NULL;
			@i @Funcdef.p_count@ = 0;
		@}
		;
ParList:	ParList ',' ID
		@{	@e ParList.lst : ParList.1.lst ID.str;
			   @ParList.lst@ = newStrList(@ID.str@, SYMBOL_DEF, SYMBOL_VARIABLE, @ParList.1.lst@);
			@i @ParList.p_count@ = @ParList.1.p_count@ + 1;
		@}
		| ParList ',' 
		@{	@e ParList.lst : ParList.1.lst;
			   @ParList.lst@ = @ParList.1.lst@;
			@i @ParList.p_count@ = @ParList.1.p_count@;
		@}
			
		| ID
		@{	@e ParList.lst : ID.str;
			   @ParList.lst@ = newStrList(@ID.str@, SYMBOL_DEF, SYMBOL_VARIABLE, NULL);
			@i @ParList.p_count@ = 1;
		@}
		;
Stats:		Labeldefs Stat ';'
		@{	@i @Stats.lst@ = concatStrList(@Stat.lst@, @Labeldefs.lst@) ;
			@i @Stats.ast@ = @Stat.ast@;
		@}
		| Stats Labeldefs Stat ';'
		@{	@i @Stats.lst@ = concatStrList(@Stat.lst@, concatStrList(@Labeldefs.lst@, @Stats.1.lst@)) ;
			@i @Stats.0.ast@ = ast_new(ASTT_STATEMENT, @Stats.1.ast@, @Stat.ast@);
		@}
		;
Labeldefs:	
		@{	@i @Labeldefs.lst@ = NULL;
		@}
		| Labeldefs ID ':'
		@{	@i @Labeldefs.lst@ = newStrList(@ID.str@, SYMBOL_DEF, SYMBOL_LABEL, @Labeldefs.1.lst@);
		@}
		;

Stat:		KW_RETURN Expr
		@{	@i @Stat.lst@ = @Expr.lst@ ;
			@i @Stat.ast@ = ast_return(@Expr.ast@);
		@}
		| KW_GOTO ID
		@{	@i @Stat.lst@ = newStrList(@ID.str@, SYMBOL_USE, SYMBOL_LABEL, NULL) ;
			/* @i @Stat.ast@ = ast_goto(@ID.str@); */
			@i @Stat.ast@ = NULL ; /* FIXME: not implemented; */
		@}
		| KW_IF Cond KW_GOTO ID 
		@{	@i @Stat.lst@ = concatStrList(newStrList(@ID.str@, SYMBOL_USE, SYMBOL_LABEL, NULL), @Cond.lst@) ;
			@i @Stat.ast@ = NULL;
		@}
		| KW_VAR ID '=' Expr /* Variablendefinition */
		@{	@i @Stat.lst@ = concatStrList(newStrList(@ID.str@, SYMBOL_DEF, SYMBOL_VARIABLE, NULL), @Expr.lst@); ;
			  
			   /* actually @Expr.lst@ should come first here in concat, because it comes after ID
			      but this would define the symbol before Expr so Expr could contain the ID */
			/* @i @Stat.ast@ = ast_varwrite(@ID.str@, @Expr.ast@) ; */
			@i @Stat.ast@ = NULL ; /* FIXME: not implemented; */
		@}
		| Term '[' Expr ']' '=' Expr /* schreibender Arrayzugriff */
		@{	@i @Stat.lst@ = concatStrList(@Expr.1.lst@, @Expr.0.lst@) ;
			/* @i @Stat.ast@ = ast_arraywrite(@Expr.1.ast@,
				ast_lea(@Term.ast@, @Expr.0.ast@));
			*/
			@i @Stat.ast@ = NULL ; /* FIXME: not implemented; */
		@}
		| ID '=' Expr /* schreibender Variablenzugriff */
		@{	@i @Stat.lst@ = concatStrList(@Expr.lst@, newStrList(@ID.str@, SYMBOL_USE, SYMBOL_VARIABLE, NULL)) ;
			/* @i @Stat.ast@ = ast_varwrite(@ID.str@, @Expr.ast@) ; */
			@i @Stat.ast@ = NULL ; /* FIXME: not implemented; */
		@}
		| Term
		@{	@i @Stat.lst@ = @Term.lst@ ;
			@i @Stat.ast@ = NULL ; /* ignore dead code */
		@}
		;
Cond:		CondAnd
		@{	@i @Cond.lst@ = @CondAnd.lst@ ; @}
		| KW_NOT Cterm
		@{	@i @Cond.lst@ = @Cterm.lst@ ; @}
		;
CondAnd:	Cterm
		@{	@i @CondAnd.lst@ = @Cterm.lst@ ; @}
		| CondAnd KW_AND Cterm
		@{	@i @CondAnd.lst@ = concatStrList(@Cterm.lst@, @CondAnd.1.lst@) ; @}
		;
Cterm:		'(' Cond ')'
		@{	@i @Cterm.lst@ = @Cond.lst@ ; @}
		| Expr OP_NE Expr
		@{	@i @Cterm.lst@ = concatStrList(@Expr.1.lst@, @Expr.0.lst@) ; @}
		| Expr '>' Expr
		@{	@i @Cterm.lst@ = concatStrList(@Expr.1.lst@, @Expr.0.lst@) ; @}
		;
Expr:		PlusTerms
		@{	@i @Expr.lst@ = @PlusTerms.lst@ ;
			@i @Expr.ast@ = @PlusTerms.ast@ ; @}
		| MulTerms
		@{	@i @Expr.lst@ = @MulTerms.lst@ ; 
			@i @Expr.ast@ = @MulTerms.ast@ ; @}
		| PreMinusTerm 
		@{	@i @Expr.lst@ = @PreMinusTerm.lst@ ; 
			@i @Expr.ast@ = @PreMinusTerm.ast@ ; @}
		;
PlusTerms:	Term '+' Term
		@{	@i @PlusTerms.lst@ = concatStrList(@Term.1.lst@, @Term.0.lst@) ; 
			@i @PlusTerms.ast@ = ast_plus(@Term.0.ast@, @Term.1.ast@) ; @}
		| PlusTerms '+' Term
		@{	@i @PlusTerms.lst@ = concatStrList(@Term.lst@, @PlusTerms.1.lst@) ; 
			@i @PlusTerms.ast@ = ast_plus(@PlusTerms.1.ast@, @Term.ast@) ; @}
		;
MulTerms:	Term '*' Term
		@{	@i @MulTerms.lst@ = concatStrList(@Term.1.lst@, @Term.0.lst@) ; 
			@i @MulTerms.ast@ = ast_mult(@Term.0.ast@, @Term.1.ast@) ; @}
		| MulTerms '*' Term
		@{	@i @MulTerms.lst@ = concatStrList(@Term.lst@, @MulTerms.1.lst@) ; 
			@i @MulTerms.ast@ = ast_mult(@MulTerms.1.ast@, @Term.ast@) ; @}
		;
PreMinusTerm:	Term
		@{	@i @PreMinusTerm.lst@ = @Term.lst@ ; 
			@i @PreMinusTerm.ast@ = @Term.ast@ ; @}
		| '-' PreMinusTerm 
		@{	@i @PreMinusTerm.0.lst@ = @PreMinusTerm.1.lst@ ; 
			@i @PreMinusTerm.0.ast@ = ast_minus(@PreMinusTerm.1.ast@) ; @}
		;
ExprList:	@{	@i @ExprList.lst@ = NULL ; @}
		| NEExprList
		@{	@i @ExprList.lst@ = @NEExprList.lst@ ; @}
		;
NEExprList:	NEExprList ',' Expr
		@{	@i @NEExprList.0.lst@ = concatStrList(@Expr.lst@, @NEExprList.1.lst@) ; @}
		| NEExprList ',' 
		@{	@i @NEExprList.0.lst@ = @NEExprList.1.lst@ ; @}
		| Expr 
		@{	@i @NEExprList.lst@ = @Expr.lst@ ; @}
		;
Term:		'(' Expr ')'
		@{	@i @Term.lst@ = @Expr.lst@ ; 
			@i @Term.ast@ = @Expr.ast@ ; @}
		| NUM
		@{	@i @Term.lst@ = NULL; 
			@i @Term.ast@ = ast_int(@NUM.value@) ; @}
		| Term '[' Expr ']' /* lesender Arrayzugriff */
		@{	@i @Term.0.lst@ = concatStrList(@Expr.lst@, @Term.1.lst@); 
			@i @Term.ast@ = ast_arrayread(ast_lea(@Term.1.ast@, @Expr.0.ast@));
		@}
		| ID /* lesender Variablenzugriff */
		@{	@i @Term.lst@ = newStrList(@ID.str@, SYMBOL_USE, SYMBOL_VARIABLE, NULL) ; 
			@i @Term.ast@ = ast_varread(@ID.str@) ; @}
		| ID '(' ExprList ')' /* Funktionsaufruf */
		@{	@i @Term.lst@ = @ExprList.lst@ ;
			/* this ID is a function !!! don't use! */
			@i @Term.ast@ = ast_int(1337) ; /* FIXME: not implemented */
		@}
		;
%%

void yyerror(const char *s)
{
	fprintf(stderr, "error: %s\n", s);
	exit(2);
}

int main()
{
	yyin = stdin;
	yyparse();
	return exitcode;
}

