%{
#include "ast.h"
#include "regalloc.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern struct RegAllocator *regallocator;

void immmov(long value, const char *dst)
{
	if(value == 0){
		printf("xor %%%s, %%%s\n", dst, dst);
	}else{
		printf("mov $%ld, %%%s\n", value, dst);
	}
}

void addto(long value, const char *dst)
{
	if(value == 0) return;
	if(value == 1){
		printf("inc %%%s\n", dst);
	}else if(value == -1){
		printf("dec %%%s\n", dst);
	}else{
		printf("add $%ld, %%%s\n", value, dst);
	}
}
void lealike(const char *mn, long off, const char *src, const char *add, long mul, const char *dst)
{
	if(strlen(add) == 0){
		assert(mul == 1);
		printf("%s %ld(%%%s), %%%s\n", mn, off, src, dst);
	}else{
		printf("%s %ld(%%%s,%%%s,%ld), %%%s\n", mn, off, src, add, mul, dst);
	}
}

void regmov(const char *src, const char *dst)
{
	if(src != dst && strcmp(src, dst) != 0){
		printf("mov %%%s, %%%s\n", src, dst);
	}
}


/* this can perform arbitrary binary operations and performs optimizations
   that work ONLY FOR COMMUTATIVE OPERATIONS!!! */
void compile_bop_comm(const char *mn, struct AST* node, const char *target)
{
	const char *dst, *src;

	//fprintf(stderr,"bop_comm: target=%s left-reg=%s right-reg=%s\n", target, node->left->reg, node->right->reg);
	if(target != NULL && target == node->left->reg){
		dst = target;
		src = node->right->reg;
		regalloc_freeiftmp(regallocator, node->right->reg);
	}else if(target != NULL && target == node->right->reg){
		dst = target;
		src = node->left->reg;
		regalloc_freeiftmp(regallocator, node->left->reg);
	}else if(regalloc_istmp(regallocator, node->left->reg)){
		dst = node->left->reg;
		src = node->right->reg;
		regalloc_freeiftmp(regallocator, node->right->reg);
	}else if(regalloc_istmp(regallocator, node->right->reg)){
		dst = node->right->reg;
		src = node->left->reg;
	}else{
		if(target != NULL)
			dst = target;
		else
			dst = regalloc_tmp(regallocator);

//		if(strcmp(mn,"add") == 0){
//			// dirty dirty dirty
//			lealike("lea", 0, node->left->reg, node->right->reg, 1, dst);
//			node->reg = dst;
//			return;
//		}
		src = node->right->reg;
		regmov(node->left->reg, dst);
	}
	printf("%s %%%s, %%%s\n", mn, src, dst);
	node->reg = dst;
}

void compile_unary(const char *mn, struct AST* node, const char *target)
{
	if(regalloc_istmp(regallocator, node->left->reg)){
		printf("%s %%%s\n", mn, node->left->reg);
		node->reg = node->left->reg;
	}else{
		const char *reg; 
		if(target != NULL)
			reg = target;
		else
			reg = regalloc_tmp(regallocator);
		regmov(node->left->reg, reg);
		printf("%s %%%s\n", mn, reg);
		node->reg = reg;
	}
}

void compile_plus(struct AST* node, const char *target)
{
	if(!regalloc_istmp(regallocator, node->left->reg) && !regalloc_istmp(regallocator, node->right->reg)){
		const char *dst = target != NULL ? target : regalloc_tmp(regallocator);
		lealike("lea", 0, node->left->reg, node->right->reg, 1, dst);
		node->reg = dst;
	}else{
		compile_bop_comm("add", node, target);
	}
}
void compile_plus_imm_left(struct AST* node, const char *target)
{
	if((target != NULL && target != node->right->reg) || !regalloc_istmp(regallocator, node->right->reg)){
		node->reg = target != NULL ? target : regalloc_tmp(regallocator);
		lealike("lea", node->left->imm, node->right->reg, "", 1, node->reg);
	}else if(regalloc_istmp(regallocator, node->right->reg)){
		node->reg = node->right->reg;
		addto(node->left->imm, node->reg);
	}
}
void compile_plus_imm_right(struct AST* node, const char *target)
{
	if((target != NULL && target != node->left->reg) || !regalloc_istmp(regallocator, node->left->reg)){
		node->reg = target != NULL ? target : regalloc_tmp(regallocator);
		lealike("lea", node->right->imm, node->left->reg, "", 1, node->reg);
	}else{
		node->reg = node->left->reg;
		addto(node->right->imm, node->reg);
	}
}


void compile_mult(struct AST* node, const char *target)
{
	compile_bop_comm("imul", node, target);
}

void compile_varwrite(struct AST* node)
{
	const char *reg = regalloc_reg(regallocator, node->n);
	regmov(node->left->reg, reg);
	node->reg = node->left->reg;
}

void compile_lea(struct AST* node)
{
	if(regalloc_istmp(regallocator, node->left->reg)){
		lealike("lea", 0, node->left->reg, node->right->reg, 8, node->left->reg);
		node->reg = node->left->reg;
		regalloc_freeiftmp(regallocator, node->right->reg);
	}else if(regalloc_istmp(regallocator, node->right->reg)){
		lealike("lea", 0, node->left->reg, node->right->reg, 8, node->right->reg);
		node->reg = node->right->reg;
	}else{
		const char *reg = regalloc_tmp(regallocator);
		lealike("lea", 0, node->left->reg, node->right->reg, 8, reg);
		node->reg = reg;
	}
}

void compile_deref_read2(struct AST *n, const char *target)
{
	struct AST* node = n->left;
	if(regalloc_istmp(regallocator, node->left->reg)){
		lealike("mov", 0, node->left->reg, node->right->reg, 8, node->left->reg);
		n->reg = node->left->reg;
		regalloc_freeiftmp(regallocator, node->right->reg);
	}else if(regalloc_istmp(regallocator, node->right->reg)){
		lealike("mov", 0, node->left->reg, node->right->reg, 8, node->right->reg);
		n->reg = node->right->reg;
	}else{
		const char *reg;
		if(target != NULL)
			reg = target;
		else
			reg = regalloc_tmp(regallocator);

		lealike("mov", 0, node->left->reg, node->right->reg, 8, reg);
		n->reg = reg;
	}
}

%}


%start stats
%term ASTT_STATEMENT=0 ASTT_GOTO=1 ASTT_RETURN=2 ASTT_VARREAD=3 ASTT_VARWRITE=4 ASTT_PLUS=5 ASTT_MINUS=6 ASTT_MULT=7 ASTT_ARRAYREAD=8 ASTT_ARRAYWRITE=9 ASTT_INT=10 ASTT_LEA=11

%%

stats:	stat					# 0
stats:  ASTT_STATEMENT(stats,stat)		# 0

stat:	retrn					# 0 # regalloc_freealltmp(regallocator);
stat:	varw					# 0 # regalloc_freealltmp(regallocator);

varw:	ASTT_VARWRITE(expr)			# 0 # compile_varwrite(bnode); 

retrn:	ASTT_RETURN(ret_expr)			# 0 # regmov(bnode->left->reg, regalloc_return_reg()); printf("ret\n");

ret_expr:	ASTT_PLUS(expr, term)		# 3 # compile_plus(bnode,regalloc_return_reg());
ret_expr:	ASTT_PLUS(term, expr)		# 3 # compile_plus(bnode,regalloc_return_reg());
ret_expr:	ASTT_PLUS(expr, ASTT_INT)	# 1 # compile_plus_imm_right(bnode,regalloc_return_reg());
ret_expr:	ASTT_PLUS(ASTT_INT, expr)	# 2 # compile_plus_imm_left(bnode,regalloc_return_reg());
ret_expr:	ASTT_MINUS(expr)		# 2 # compile_unary("neg", bnode,regalloc_return_reg());
ret_expr:	ASTT_MINUS(ASTT_MINUS(expr))	# 0 # bnode->reg = bnode->left->left->reg;
ret_expr:	ASTT_MINUS(ASTT_INT)		# 1 # immmov((-1)*bnode->left->imm, bnode->reg = regalloc_return_reg());
ret_expr:	ASTT_MULT(expr, term)		# 2 # compile_mult(bnode,regalloc_return_reg());
ret_expr:	ASTT_MULT(term, expr)		# 2 # compile_mult(bnode,regalloc_return_reg());
ret_expr:	ASTT_INT			# 1 # immmov(bnode->imm, bnode->reg = regalloc_return_reg());
ret_expr:	ASTT_VARREAD			# 1 # regmov(regalloc_reg(regallocator, bnode->n), bnode->reg = regalloc_return_reg());
ret_expr:	ASTT_ARRAYREAD(s_addr)		# 1 # compile_deref_read2(bnode, bnode->reg = regalloc_return_reg());
ret_expr:	expr				# 3

expr:	ASTT_PLUS(expr, term)			# 3 # compile_plus(bnode,NULL);
expr:	ASTT_PLUS(term, expr)			# 3 # compile_plus(bnode,NULL);
expr:	ASTT_PLUS(expr, ASTT_INT)		# 1 # compile_plus_imm_right(bnode,NULL);
expr:	ASTT_PLUS(ASTT_INT, expr)		# 2 # compile_plus_imm_left(bnode,NULL);
expr:	ASTT_MINUS(expr)			# 1 # compile_unary("neg", bnode,NULL);
expr:	ASTT_MINUS(ASTT_MINUS(expr))		# 0 # bnode->reg = bnode->left->left->reg;
expr:	ASTT_MINUS(ASTT_INT)			# 1 # immmov((-1)*bnode->left->imm, bnode->reg = regalloc_tmp(regallocator));
expr:	ASTT_MULT(expr, term)			# 1 # compile_mult(bnode,NULL);
expr:	ASTT_MULT(term, expr)			# 1 # compile_mult(bnode,NULL);
expr:	term					# 0

term:	imm					# 0
term:	expr					# 10

imm:	ASTT_INT				# 1 # immmov(bnode->imm, bnode->reg = regalloc_tmp(regallocator));
imm:	ASTT_VARREAD				# 1 # bnode->reg = regalloc_reg(regallocator, bnode->n);
imm:	ASTT_ARRAYREAD(s_addr)			# 1 # compile_deref_read2(bnode, NULL);

s_addr:	ASTT_LEA(expr,expr)			# 0


