#include "ast.h"

struct AST* ast_new(enum ASTT type, struct AST *left, struct AST *right)
{
	struct AST *ast = malloc(sizeof(struct AST));
	ast->type = type;
	ast->left = left;
	ast->right = right;
	ast->reg = NULL;
	ast->n = NULL;
}

struct AST* ast_return(struct AST *left)
{
	return ast_new(ASTT_RETURN, left, NULL);
}

struct AST* ast_goto(const char *n)
{
	struct AST *ast = ast_new(ASTT_GOTO, NULL, NULL);
	ast->n = n;
	return ast;
}

struct AST* ast_plus(struct AST *left, struct AST *right)
{
	return ast_new(ASTT_PLUS, left, right);
}
struct AST* ast_mult(struct AST *left, struct AST *right)
{
	return ast_new(ASTT_MULT, left, right);
}
struct AST* ast_minus(struct AST *left)
{
	return ast_new(ASTT_MINUS, left, NULL);
}

struct AST* ast_int(int64_t x)
{
	struct AST *ast = ast_new(ASTT_INT, NULL, NULL);
	ast->imm = x;
	return ast;
}

struct AST* ast_varwrite(const char *n, struct AST *left)
{
	struct AST *ast = ast_new(ASTT_VARWRITE, left, NULL);
	ast->n = n;
	return ast;
}

struct AST* ast_varread(const char *s)
{
	struct AST *ast = ast_new(ASTT_VARREAD, NULL, NULL);
	ast->n = s;
	return ast;
}

struct AST* ast_arraywrite(struct AST *left, struct AST *right)
{
	return ast_new(ASTT_ARRAYWRITE, left, right);
}

struct AST* ast_arrayread(struct AST* left)
{
	return ast_new(ASTT_ARRAYREAD, left, NULL);
}

struct AST* ast_lea(struct AST* left, struct AST* right)
{
	return ast_new(ASTT_LEA, left, right);
}
