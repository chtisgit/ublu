	.file	"angabe.c"
	.text
.globl asmb
	.type	asmb, @function
asmb:
.LFB0:
	.cfi_startproc

	# 1. parameter: rdi ... x[]
	# 2. parameter: rsi ... n
	# 3. parameter: rdx ... y
	# 4. parameter: rcx ... a[]

	xchg	%rsi, %rcx
	xchg	%rdi, %rsi
	mov	%rdx, %r8

	# rsi	... x[]
	# r8	... y
	# rdi	... a[]
	# rcx	... n

	# set a[n] = 0 to account for case n==0 and
	# to clear last array entry for last unrolled
	# loop iteration
	movq	$0, (%rdi,%rcx,8)

	# if n == 0 -> return
	test	%rcx, %rcx
	jz	.fn_exit

	dec	%rcx

	xor	%r9, %r9	# zero register
	xor	%r10, %r10	# carry flag save register
	
	# clear direction flag due to use of lodsq/stosq
	# it should be cleared by default but let's do this cleanly
	cld

	# 1. loop iteration unrolled
	# we don't need addition here
	lodsq
	mul	%r8
	stosq

	# main loop is iterated %rcx times
	jrcxz	.nolp
.lp:	push	%rdx
	lodsq
	mul	%r8
	stosq

	# restoring the last high qword of multiplication
	pop	%rax
	
	# add last carry flag to rax
	add	%r10, %rax
	# add to the value saved in this iteration the
	# high qword from the last multiplication
	adc	%rax, -8(%rdi)
	# save the carry flag
	mov	%r9, %r10
	adc	%r10, %r10
	loop	.lp
.nolp:
	# last loop iteration unrolled
	# no loading/multiplying in last iteration
	add	%r10, %rdx
	adc	%rdx, (%rdi)

.fn_exit:
	ret
	.cfi_endproc
	.byte 0x46, 0x69, 0x65, 0x64, 0x6c, 0x65, 0x72
.LFE0:
	.size	asmb, .-asmb
	.ident	"GCC: (Debian 4.4.5-8) 4.4.5"
	.section	.note.GNU-stack,"",@progbits
