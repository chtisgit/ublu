#pragma once
#include <assert.h>
#include <stdlib.h>


#define REGALLOC_MAXREGS 9
struct RegAllocator{
	const char *a[REGALLOC_MAXREGS];
	int p_count;
	int addvar;
	char inuse[REGALLOC_MAXREGS+1];
};

struct RegAllocator regalloc_new();
const char* regalloc_return_reg();
const char* return_reg_name();
const char* regalloc_parameter(struct RegAllocator *r, const char *name);
const char* regalloc_var(struct RegAllocator *r, const char *name);
const char* regalloc_tmp(struct RegAllocator *r);
int regalloc_istmp(struct RegAllocator *r, const char *s);
void regalloc_freetmp(struct RegAllocator *r, const char *s);
void regalloc_freeiftmp(struct RegAllocator *r, const char *s);
void regalloc_freealltmp(struct RegAllocator *r);
const char* regalloc_reg(struct RegAllocator *r, const char *name);
const char* regalloc_paramreg(int n);
int regalloc_locals(const struct RegAllocator *const r, const char *regs[9]);
