#pragma once
#include "ast.h"
#include "strlist.h"

#define TREENULL		((struct AST *)NULL)
#define TREESIZE		(sizeof(struct AST))
#define TREECAST		struct AST*
#define NODEPTR_TYPE		struct AST*
#define OP_LABEL(node)		((node)->type)
#define LEFT_CHILD(node)	((node)->left)
#define RIGHT_CHILD(node)	((node)->right)
#define STATE_LABEL(node)	((node)->label)
#define PANIC(...)		fprintf(stderr, __VA_ARGS__)

extern struct RegAllocator *regallocator;
extern unsigned exitlabel;

unsigned gen_tmp_label();
void function(const char *name, struct StrList *lst, struct AST *ast, int p_count);
void immmov(long value, const char *dst);
void addto(long value, const char *dst);
void lealike(const char *mn, long off, const char *src, const char *add, long mul, const char *dst);
void regmov(const char *src, const char *dst);
void store(const char *mn, const char *src, long off, const char *dst, const char *add, long mul);
void jmp(const char *mn, const char *label);
void label(const char *label);
void tmplabel(unsigned x);
void compile_bop_comm(const char *mn, struct AST* node, const char *target);
void compile_unary(const char *mn, struct AST* node, const char *target);
void compile_plus(struct AST* node, const char *target);
void compile_plus_imm_left(struct AST* node, const char *target);
void compile_plus_imm_right(struct AST* node, const char *target);
void compile_mult(struct AST* node, const char *target);
void compile_varwrite(struct AST* node);
void compile_lea(struct AST* node);
void compile_deref_read2(struct AST *n, const char *target);
void compile_const_deref_read(struct AST *n, const char *target);
void compile_deref_write(struct AST *n);
void compile_const_deref_write(struct AST *n);
void compile_not(struct AST *n);
void compile_notnot(struct AST *n);
void compareimm_left(const char *mn, struct AST *n);
void compareimm_right(const char *mn, struct AST *n);
void comparereg(const char *mn, struct AST *n);
void freesavedregs();
