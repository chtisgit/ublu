%{
#include "strlist.h"
#include "codegen.h"
#include "ast.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern int exitcode;

#define YYSTYPE char*

void yyerror(const char *s);
extern char *yytext;
extern FILE *yyin;

%}

%token KW_IF
%token KW_AND
%token KW_END
%token KW_NOT
%token KW_VAR
%token KW_GOTO
%token KW_RETURN
%token OP_NE
%token ID 
%token NUM

/* @attributes { struct ST *st; } ParList */
@attributes { struct StrList *lst; struct AST *ast; unsigned true_label, false_label; unsigned inv; } Cond CondAnd Cterm
@attributes { struct StrList *lst; int p_count; } ParList
@attributes { struct StrList *lst; struct AST *ast; } Stats Stat Expr PlusTerms MulTerms PreMinusTerm Term Labeldefs 
@attributes { struct StrList *lst; struct AST *ast; int push_n; } ExprList
@attributes { struct StrList *lst; struct AST *ast; char *name; int p_count; } Funcdef
@attributes { char *str; } ID
@attributes { long value; } NUM
@traversal @lefttoright @preorder pre
@traversal @lefttoright @postorder post 

%%

Start:		Program;
Program:	
		| Funcdefs
		;
Funcdefs:	Funcdef ';'
		@{
			@post function(@Funcdef.name@, @Funcdef.lst@, @Funcdef.ast@, @Funcdef.p_count@);
		@}
		| Funcdefs Funcdef ';'
		@{	
			@post function(@Funcdef.name@, @Funcdef.lst@, @Funcdef.ast@, @Funcdef.p_count@);
		@}
		;
Funcdef:	ID '(' ParList ')' Stats KW_END /* Funktionsdefinition */
		@{	@i @Funcdef.lst@ = concatStrList(@Stats.lst@, @ParList.lst@);
			@i @Funcdef.name@ = @ID.0.str@;
			@i @Funcdef.ast@ = @Stats.ast@;
			@i @Funcdef.p_count@ = @ParList.p_count@;
		@}
		| ID '(' ')' Stats KW_END
		@{	@i @Funcdef.lst@ = @Stats.lst@;
			@i @Funcdef.name@ = @ID.0.str@;
			@i @Funcdef.ast@ = @Stats.ast@;
			@i @Funcdef.p_count@ = 0;
		@}
		| ID '(' ParList ')' KW_END
		@{	@i @Funcdef.lst@ = @ParList.lst@;
			@i @Funcdef.name@ = @ID.0.str@;
			@i @Funcdef.ast@ = NULL;
			@i @Funcdef.p_count@ = @ParList.p_count@;
		@}
		| ID '(' ')' KW_END
		@{	@i @Funcdef.lst@ = NULL;
			@i @Funcdef.name@ = @ID.0.str@;
			@i @Funcdef.ast@ = NULL;
			@i @Funcdef.p_count@ = 0;
		@}
		;
ParList:	ParList ',' ID
		@{	@e ParList.lst : ParList.1.lst ID.str;
			   @ParList.lst@ = newStrList(@ID.str@, SYMBOL_DEF, SYMBOL_VARIABLE, @ParList.1.lst@);
			@i @ParList.p_count@ = @ParList.1.p_count@ + 1;
		@}
		| ParList ',' 
		@{	@e ParList.lst : ParList.1.lst;
			   @ParList.lst@ = @ParList.1.lst@;
			@i @ParList.p_count@ = @ParList.1.p_count@;
		@}
			
		| ID
		@{	@e ParList.lst : ID.str;
			   @ParList.lst@ = newStrList(@ID.str@, SYMBOL_DEF, SYMBOL_VARIABLE, NULL);
			@i @ParList.p_count@ = 1;
		@}
		;
Stats:		Labeldefs Stat ';'
		@{	@i @Stats.lst@ = concatStrList(@Stat.lst@, @Labeldefs.lst@) ;
			@i @Stats.ast@ = ast_statement(@Labeldefs.ast@, @Stat.ast@, NULL) ;
		@}
		| Stats Labeldefs Stat ';'
		@{	@i @Stats.lst@ = concatStrList(@Stat.lst@, concatStrList(@Labeldefs.lst@, @Stats.1.lst@)) ;
			@i @Stats.ast@ = ast_statement(@Labeldefs.ast@, @Stat.ast@, @Stats.1.ast@) ;
		@}
		;
Labeldefs:	
		@{	@i @Labeldefs.lst@ = NULL;
			@i @Labeldefs.ast@ = NULL;
		@}
		| Labeldefs ID ':'
		@{	@i @Labeldefs.lst@ = newStrList(@ID.str@, SYMBOL_DEF, SYMBOL_LABEL, @Labeldefs.1.lst@);
			@i @Labeldefs.ast@ = ast_label(@ID.str@);
		@}
		;

Stat:		KW_RETURN Expr
		@{	@i @Stat.lst@ = @Expr.lst@ ;
			@i @Stat.ast@ = ast_return(@Expr.ast@);
		@}
		| KW_GOTO ID
		@{	@i @Stat.lst@ = newStrList(@ID.str@, SYMBOL_USE, SYMBOL_LABEL, NULL) ;
			@i @Stat.ast@ = ast_goto(@ID.str@);
		@}
		| KW_IF Cond KW_GOTO ID 
		@{	@i @Stat.lst@ = concatStrList(newStrList(@ID.str@, SYMBOL_USE, SYMBOL_LABEL, NULL), @Cond.lst@) ;
			@i @Cond.true_label@ = gen_tmp_label() ;
			@i @Cond.false_label@ = gen_tmp_label() ;
			@i @Cond.inv@ = 0 ;
			@i @Stat.ast@ = ast_if(@Cond.ast@, @ID.str@, @Cond.true_label@, @Cond.false_label@) ;
		@}
		| KW_VAR ID '=' Expr /* Variablendefinition */
		@{	@i @Stat.lst@ = concatStrList(newStrList(@ID.str@, SYMBOL_DEF, SYMBOL_VARIABLE, NULL), @Expr.lst@); ;
			  
			   /* actually @Expr.lst@ should come first here in concat, because it comes after ID
			      but this would define the symbol before Expr so Expr could contain the ID */
			@i @Stat.ast@ = ast_varwrite(@ID.str@, @Expr.ast@) ;
		@}
		| Term '[' Expr ']' '=' Expr /* schreibender Arrayzugriff */
		@{	@i @Stat.lst@ = concatStrList(@Expr.1.lst@, @Expr.0.lst@) ;
			@i @Stat.ast@ = ast_arraywrite(@Expr.1.ast@,
				ast_lea(@Term.ast@, @Expr.0.ast@));
		@}
		| ID '=' Expr /* schreibender Variablenzugriff */
		@{	@i @Stat.lst@ = concatStrList(@Expr.lst@, newStrList(@ID.str@, SYMBOL_USE, SYMBOL_VARIABLE, NULL)) ;
			@i @Stat.ast@ = ast_varwrite(@ID.str@, @Expr.ast@) ;
		@}
		| Term
		@{	@i @Stat.lst@ = @Term.lst@ ;
			@i @Stat.ast@ = @Term.ast@ ; /* ignore dead code */
		@}
		;
Cond:		CondAnd
		@{	@i @Cond.lst@ = @CondAnd.lst@ ; 
			@i @Cond.ast@ = @CondAnd.ast@ ;
			@i @CondAnd.inv@ = @Cond.inv@ ;
			@i @CondAnd.true_label@ = @Cond.true_label@ ;
			@i @CondAnd.false_label@ = @Cond.false_label@ ; @}
		| KW_NOT Cterm
		@{	@i @Cond.lst@ = @Cterm.lst@ ; 
			@e Cond.ast : Cterm.ast Cond.true_label Cond.false_label Cond.inv;
			   @Cond.ast@ = ast_cnot(@Cterm.ast@, @Cond.true_label@, @Cond.false_label@, !@Cond.inv@) ; 
			@i @Cterm.inv@ = !@Cond.inv@ ;
			@i @Cterm.true_label@ = gen_tmp_label() ;
			@i @Cterm.false_label@ = gen_tmp_label() ; 
			@}
		;
CondAnd:	Cterm
		@{	@i @CondAnd.lst@ = @Cterm.lst@ ; 
			@i @CondAnd.ast@ = @Cterm.ast@ ;
			@i @Cterm.inv@ = @CondAnd.inv@ ;
			@i @Cterm.true_label@ = @CondAnd.true_label@ ;
			@i @Cterm.false_label@ = @CondAnd.false_label@ ; @}
		| CondAnd KW_AND Cterm
		@{	@i @CondAnd.lst@ = concatStrList(@Cterm.lst@, @CondAnd.1.lst@) ; 
			@i @CondAnd.ast@ = ast_cand(@CondAnd.1.ast@, @Cterm.ast@, @CondAnd.true_label@, @CondAnd.false_label@) ;
			@i @Cterm.inv@ = @CondAnd.inv@ ;
			@i @Cterm.true_label@ = @CondAnd.true_label@ ; 
			@i @Cterm.false_label@ = @CondAnd.false_label@ ; 
			@i @CondAnd.1.inv@ = @CondAnd.inv@ ;
			@i @CondAnd.1.true_label@ = @CondAnd.true_label@ ; 
			@i @CondAnd.1.false_label@ = @CondAnd.false_label@ ; @}
		;
Cterm:		'(' Cond ')'
		@{	@i @Cterm.lst@ = @Cond.lst@ ; 
			@i @Cterm.ast@ = @Cond.ast@ ;
			@i @Cond.inv@ = @Cterm.inv@ ; 
			@i @Cond.true_label@ = @Cterm.true_label@ ;
			@i @Cond.false_label@ = @Cterm.false_label@ ; @}
		| Expr OP_NE Expr
		@{	@i @Cterm.lst@ = concatStrList(@Expr.1.lst@, @Expr.0.lst@) ; 
			@i @Cterm.ast@ = ast_cneq(@Expr.0.ast@, @Expr.1.ast@,
				@Cterm.true_label@, @Cterm.false_label@) ; @}
		| Expr '>' Expr
		@{	@i @Cterm.lst@ = concatStrList(@Expr.1.lst@, @Expr.0.lst@) ;
			@i @Cterm.ast@ = ast_cg(@Expr.0.ast@, @Expr.1.ast@,
				@Cterm.true_label@, @Cterm.false_label@) ; @}
		;
Expr:		PlusTerms
		@{	@i @Expr.lst@ = @PlusTerms.lst@ ;
			@i @Expr.ast@ = @PlusTerms.ast@ ; @}
		| MulTerms
		@{	@i @Expr.lst@ = @MulTerms.lst@ ; 
			@i @Expr.ast@ = @MulTerms.ast@ ; @}
		| PreMinusTerm 
		@{	@i @Expr.lst@ = @PreMinusTerm.lst@ ; 
			@i @Expr.ast@ = @PreMinusTerm.ast@ ; @}
		;
PlusTerms:	Term '+' Term
		@{	@i @PlusTerms.lst@ = concatStrList(@Term.1.lst@, @Term.0.lst@) ; 
			@i @PlusTerms.ast@ = ast_plus(@Term.0.ast@, @Term.1.ast@) ; @}
		| PlusTerms '+' Term
		@{	@i @PlusTerms.lst@ = concatStrList(@Term.lst@, @PlusTerms.1.lst@) ; 
			@i @PlusTerms.ast@ = ast_plus(@PlusTerms.1.ast@, @Term.ast@) ; @}
		;
MulTerms:	Term '*' Term
		@{	@i @MulTerms.lst@ = concatStrList(@Term.1.lst@, @Term.0.lst@) ; 
			@i @MulTerms.ast@ = ast_mult(@Term.0.ast@, @Term.1.ast@) ; @}
		| MulTerms '*' Term
		@{	@i @MulTerms.lst@ = concatStrList(@Term.lst@, @MulTerms.1.lst@) ; 
			@i @MulTerms.ast@ = ast_mult(@MulTerms.1.ast@, @Term.ast@) ; @}
		;
PreMinusTerm:	Term
		@{	@i @PreMinusTerm.lst@ = @Term.lst@ ; 
			@i @PreMinusTerm.ast@ = @Term.ast@ ; @}
		| '-' PreMinusTerm 
		@{	@i @PreMinusTerm.0.lst@ = @PreMinusTerm.1.lst@ ; 
			@i @PreMinusTerm.0.ast@ = ast_minus(@PreMinusTerm.1.ast@) ; @}
		;
ExprList:	
		@{	@i @ExprList.lst@ = NULL ;
			@i @ExprList.ast@ = ast_callersave() ; @}
		| Expr
		@{	@i @ExprList.lst@ = concatStrList(NULL, @Expr.lst@) ; 
			@i @ExprList.ast@ = ast_push(ast_callersave(), @Expr.ast@, @ExprList.push_n@) ; @}
		| Expr ',' ExprList
		@{	@i @ExprList.lst@ = concatStrList(@Expr.lst@, @ExprList.1.lst@) ; 
			@i @ExprList.1.push_n@ = @ExprList.push_n@+1 ;
			@i @ExprList.ast@ = ast_push(@ExprList.1.ast@, @Expr.ast@, @ExprList.push_n@) ; @}
		;
Term:		'(' Expr ')'
		@{	@i @Term.lst@ = @Expr.lst@ ; 
			@i @Term.ast@ = @Expr.ast@ ; @}
		| NUM
		@{	@i @Term.lst@ = NULL; 
			@i @Term.ast@ = ast_int(@NUM.value@) ; @}
		| Term '[' Expr ']' /* lesender Arrayzugriff */
		@{	@i @Term.0.lst@ = concatStrList(@Expr.lst@, @Term.1.lst@); 
			@i @Term.ast@ = ast_arrayread(ast_lea(@Term.1.ast@, @Expr.0.ast@));
		@}
		| ID /* lesender Variablenzugriff */
		@{	@i @Term.lst@ = newStrList(@ID.str@, SYMBOL_USE, SYMBOL_VARIABLE, NULL) ; 
			@i @Term.ast@ = ast_varread(@ID.str@) ; @}
		| ID '(' ExprList ')' /* Funktionsaufruf */
		@{	@i @Term.lst@ = @ExprList.lst@ ;
			/* this ID is a function !!! don't use! */
			@i @Term.ast@ = ast_call(@ExprList.ast@, ast_int(1337), @ID.str@) ; 
			@i @ExprList.push_n@ = 0;
		@}
		;
%%

void yyerror(const char *s)
{
	fprintf(stderr, "error: %s\n", s);
	exit(2);
}

int main()
{
	yyin = stdin;
	yyparse();
	return exitcode;
}

