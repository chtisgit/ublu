#include "strlist.h"

struct StrList *newStrList(char *word, enum SymbolOcc occ, enum SymbolType typ, struct StrList *next)
{
	struct StrList *lst = malloc(sizeof(struct StrList));
	lst->s = word;
	lst->typ = typ;
	lst->occ = occ;
	lst->next = next;
	return lst;
}

struct StrList *concatStrList(struct StrList *x, struct StrList *y)
{
	struct StrList **z = &x;
	while(*z != NULL){
		z = &((*z)->next);
	}
	*z = y;
	return x;
}

void printStrList(const struct StrList *x)
{
	if(x == NULL){
		printf("StrList: (empty)\n");
		return;
	}
	printf("StrList:\n");
	do{
		printf(" - %s \t %s %s\n", x->s, 
			x->occ == SYMBOL_USE ? "USE" : "DEF",
			x->typ == SYMBOL_LABEL ? "LABEL" : "VARIABLE");
		x = x->next;
	}while(x != NULL);
}

struct StrList* existsStrEx(struct StrList *x, const char *w, int occ, int typ)
{
	assert(w != NULL);
	if(x != NULL){
		//printf("looking for %s %d %d ...\n", w, occ, typ);
		do{
			//printf("   %s %d %d \n", x->s, x->occ, x->typ);
			if(((x->occ == occ && occ != SYMBOL_ANY) || occ == SYMBOL_ANY) &&
			   ((x->typ == typ && typ != SYMBOL_ANY) || typ == SYMBOL_ANY) != 0 && strcmp(x->s, w) == 0){
				break;
			}
			x = x->next;
		}while(x != NULL);
	}
	return x;
}
struct StrList* existsStr(struct StrList *x, const char *w)
{
	return existsStrEx(x, w, SYMBOL_ANY, SYMBOL_ANY);
}

/* return 0 -> there were errors */
int checkStrList(struct StrList *x)
{
	struct StrList *top = x;
	while(x != NULL){
		const struct StrList *y;
		switch(x->typ){
		case SYMBOL_VARIABLE:
			if(x->occ == SYMBOL_DEF && (y = existsStr(x->next, x->s)) != NULL){
				if(y->occ == SYMBOL_DEF){
					fprintf(stderr,"error: variable %s is defined multiply\n", x->s);
				}else{
					fprintf(stderr,"error: variable %s used before definition\n", x->s);
				}
				return 0;
			}else if(x->occ == SYMBOL_USE && existsStrEx(x->next, x->s, SYMBOL_DEF, SYMBOL_VARIABLE) == NULL){
				fprintf(stderr, "error: variable %s used but not defined\n", x->s);
				return 0;
			}
			break;
		case SYMBOL_LABEL:
			if(x->occ == SYMBOL_DEF && (y = existsStrEx(x->next, x->s, SYMBOL_DEF, SYMBOL_ANY)) != NULL){
				if(y->typ == SYMBOL_LABEL){
					fprintf(stderr,"error: label %s is defined multiply\n", x->s);
				}else if(y->typ == SYMBOL_VARIABLE){
					fprintf(stderr,"error: variable %s cannot be used as jump target\n", x->s);
				}
				return 0;
			}else if(x->occ == SYMBOL_USE && existsStrEx(top, x->s, SYMBOL_DEF, SYMBOL_LABEL) == NULL){
				fprintf(stderr, "error: label %s used but not defined\n", x->s);
				return 0;
			}
			break;
		default:
			assert(0);
			break;
		}
		x = x->next;
	}
	return 1;
}
