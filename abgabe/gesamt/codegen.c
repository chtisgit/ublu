#include "codegen.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "regalloc.h"
#include "ast.h"

extern int exitcode;
struct RegAllocator *regallocator = NULL;

unsigned gen_tmp_label()
{
	static unsigned L = 0;
	return ++L;
}

void func_header(const char *name)
{
	printf(".globl %s\n.type %s, @function\n%s:\n", name, name, name);
}

void func_footer(const char *name)
{
	printf("ret # end function %s\n", name);
}


static void reserve_vars(struct RegAllocator *a, struct StrList *lst, struct AST *ast, int p_count)
{
	int p = 0, remaining;
	struct StrList *x, *xx, *last = NULL;

	for(;;){
		const char *reg;

		remaining = 0;
		for(x = lst; x != last; x = x->next){
			if(x->occ == SYMBOL_DEF && x->typ == SYMBOL_VARIABLE){
				xx = x;
				remaining++;
			}
		}
		if(remaining == 0) break;

		last = xx;
		if(p < p_count){
			reg = regalloc_parameter(a, xx->s);
			p++;
			//fprintf(stderr, "registered parameter %s\n", xx->s);
		}else{
			reg = regalloc_var(a, xx->s);
			//fprintf(stderr, "registered variable %s\n", xx->s);
		}

	}

	assert(p == p_count);
}

static void indent(int n)
{
	while(--n >= 0){
		fputc(' ',stderr);
	}
}

static void traverse_tree(struct AST* ast, int n)
{
	if(ast == NULL) return;
	indent(n);
	fprintf(stderr, "+ %d\n", ast->type);
	traverse_tree(ast->left, n+1);
	traverse_tree(ast->right, n+1);
}

static int has_function_call(struct AST* ast)
{
	if(ast == NULL) return 0;
	if(ast->type == ASTT_CALL) return 1;
	return has_function_call(ast->right) || has_function_call(ast->left);
}

void gen_stackframe(struct RegAllocator *r)
{
	printf(
	"push %%rbp\n"
	"mov %%rsp, %%rbp\n"
	"sub $%d, %%rsp\n",
	(r->p_count+r->addvar)*8);
}
void destroy_stackframe()
{
	printf("leave\n");
}

unsigned exitlabel;

void function(const char *name, struct StrList *lst, struct AST *ast, int p_count)
{
	struct RegAllocator a = regalloc_new();
	int use_stackframe;

	exitlabel = gen_tmp_label();

	regallocator = &a;
	if(checkStrList(lst) == 0){
		exitcode = 3;
		return;
	}

	reserve_vars(&a, lst, ast, p_count);
	use_stackframe = has_function_call(ast) && (a.addvar+a.p_count > 0);
	func_header(name);
	if(use_stackframe)
		gen_stackframe(&a);

	//traverse_tree(ast, 0);

	if(ast != NULL){
		burm_label(ast);
		burm_reduce(ast, 1);
	}

	tmplabel(exitlabel);
	if(use_stackframe)
		destroy_stackframe(&a);

	func_footer(name);

	regallocator = NULL;
}


extern struct RegAllocator *regallocator;

void immmov(long value, const char *dst)
{
	if(value == 0){
		printf("xor %%%s, %%%s\n", dst, dst);
	}else{
		printf("mov $%ld, %%%s\n", value, dst);
	}
}

void addto(long value, const char *dst)
{
	if(value == 0) return;
	if(value == 1){
		printf("inc %%%s\n", dst);
	}else if(value == -1){
		printf("dec %%%s\n", dst);
	}else{
		printf("add $%ld, %%%s\n", value, dst);
	}
}
void lealike(const char *mn, long off, const char *src, const char *add, long mul, const char *dst)
{
	if(strlen(add) == 0){
		assert(mul == 1);
		printf("%s %ld(%%%s), %%%s\n", mn, off, src, dst);
	}else{
		printf("%s %ld(%%%s,%%%s,%ld), %%%s\n", mn, off, src, add, mul, dst);
	}
}
void store(const char *mn, const char *src, long off, const char *dst, const char *add, long mul)
{
	assert(mul == 1 || mul == 2 || mul == 4 || mul == 8);
	if(strlen(add) == 0){
		assert(mul == 1);
		printf("%s %%%s, %ld(%%%s)\n", mn, src, off, dst);
	}else{
		printf("%s %%%s, %ld(%%%s,%%%s,%ld)\n", mn, src, off, dst, add, mul);
	}
}

void regmov(const char *src, const char *dst)
{
	if(src != dst && strcmp(src, dst) != 0){
		printf("mov %%%s, %%%s\n", src, dst);
	}
}

#define LABEL_FORMATSTR(F)	".L" F

void jmp(const char *mn, const char *label)
{
	printf("%s " LABEL_FORMATSTR("%s") "\n", mn, label);
}
void label(const char *label)
{
	printf(LABEL_FORMATSTR("%s") ":\n", label);
}
void jmptmp(const char *mn, unsigned label)
{
	printf("%s " LABEL_FORMATSTR("%u") "\n", mn, label);
}
void tmplabel(unsigned x)
{
	printf(LABEL_FORMATSTR("%u") ":\n", x);
}


/* this can perform arbitrary binary operations and performs optimizations
   that work ONLY FOR COMMUTATIVE OPERATIONS!!! */
void compile_bop_comm(const char *mn, struct AST* node, const char *target)
{
	const char *dst, *src;

	if(target != NULL && target == node->left->reg){
		dst = target;
		src = node->right->reg;
		regalloc_freeiftmp(regallocator, node->right->reg);
	}else if(target != NULL && target == node->right->reg){
		dst = target;
		src = node->left->reg;
		regalloc_freeiftmp(regallocator, node->left->reg);
	}else if(regalloc_istmp(regallocator, node->left->reg)){
		dst = node->left->reg;
		src = node->right->reg;
		regalloc_freeiftmp(regallocator, node->right->reg);
	}else if(regalloc_istmp(regallocator, node->right->reg)){
		dst = node->right->reg;
		src = node->left->reg;
	}else{
		if(target != NULL)
			dst = target;
		else
			dst = regalloc_tmp(regallocator);

		src = node->right->reg;
		regmov(node->left->reg, dst);
	}
	printf("%s %%%s, %%%s\n", mn, src, dst);
	node->reg = dst;
}

void compile_unary(const char *mn, struct AST* node, const char *target)
{
	if(regalloc_istmp(regallocator, node->left->reg)){
		printf("%s %%%s\n", mn, node->left->reg);
		node->reg = node->left->reg;
	}else{
		const char *reg; 
		if(target != NULL)
			reg = target;
		else
			reg = regalloc_tmp(regallocator);
		regmov(node->left->reg, reg);
		printf("%s %%%s\n", mn, reg);
		node->reg = reg;
	}
}

void compile_plus(struct AST* node, const char *target)
{
	if(!regalloc_istmp(regallocator, node->left->reg) && !regalloc_istmp(regallocator, node->right->reg)){
		const char *dst = target != NULL ? target : regalloc_tmp(regallocator);
		lealike("lea", 0, node->left->reg, node->right->reg, 1, dst);
		node->reg = dst;
	}else{
		compile_bop_comm("add", node, target);
	}
}
void compile_plus_imm_left(struct AST* node, const char *target)
{
	if((target != NULL && target != node->right->reg) || !regalloc_istmp(regallocator, node->right->reg)){
		node->reg = target != NULL ? target : regalloc_tmp(regallocator);
		lealike("lea", node->left->imm, node->right->reg, "", 1, node->reg);
	}else if(regalloc_istmp(regallocator, node->right->reg)){
		node->reg = node->right->reg;
		addto(node->left->imm, node->reg);
	}
}
void compile_plus_imm_right(struct AST* node, const char *target)
{
	if((target != NULL && target != node->left->reg) || !regalloc_istmp(regallocator, node->left->reg)){
		node->reg = target != NULL ? target : regalloc_tmp(regallocator);
		lealike("lea", node->right->imm, node->left->reg, "", 1, node->reg);
	}else{
		node->reg = node->left->reg;
		addto(node->right->imm, node->reg);
	}
}


void compile_mult(struct AST* node, const char *target)
{
	compile_bop_comm("imul", node, target);
}

void compile_varwrite(struct AST* node)
{
	const char *reg = regalloc_reg(regallocator, node->n);
	regmov(node->left->reg, reg);
	node->reg = node->left->reg;
}

void compile_lea(struct AST* node)
{
	if(regalloc_istmp(regallocator, node->left->reg)){
		lealike("lea", 0, node->left->reg, node->right->reg, 8, node->left->reg);
		node->reg = node->left->reg;
		regalloc_freeiftmp(regallocator, node->right->reg);
	}else if(regalloc_istmp(regallocator, node->right->reg)){
		lealike("lea", 0, node->left->reg, node->right->reg, 8, node->right->reg);
		node->reg = node->right->reg;
	}else{
		const char *reg = regalloc_tmp(regallocator);
		lealike("lea", 0, node->left->reg, node->right->reg, 8, reg);
		node->reg = reg;
	}
}

void compile_deref_read2(struct AST *n, const char *target)
{
	struct AST* node = n->left;
	if(regalloc_istmp(regallocator, node->left->reg)){
		lealike("mov", 0, node->left->reg, node->right->reg, 8, node->left->reg);
		n->reg = node->left->reg;
		regalloc_freeiftmp(regallocator, node->right->reg);
	}else if(regalloc_istmp(regallocator, node->right->reg)){
		lealike("mov", 0, node->left->reg, node->right->reg, 8, node->right->reg);
		n->reg = node->right->reg;
	}else{
		const char *reg;
		if(target != NULL)
			reg = target;
		else
			reg = regalloc_tmp(regallocator);

		lealike("mov", 0, node->left->reg, node->right->reg, 8, reg);
		n->reg = reg;
	}
}

void compile_const_deref_read(struct AST *n, const char *target)
{
	struct AST* node = n->left;
	if(target != NULL || !regalloc_istmp(regallocator, node->left->reg)){
		const char *reg;
		if(target != NULL)
			reg = target;
		else
			reg = regalloc_tmp(regallocator);

		lealike("mov", node->right->imm*8, node->left->reg, "", 1, reg);
		n->reg = reg;
	}else{
		lealike("mov", node->right->imm*8, node->left->reg, "", 1, node->left->reg);
		n->reg = node->left->reg;
	}
}

void compile_deref_write(struct AST *n)
{
	struct AST* node = n->right;
	store("mov", n->left->reg, 0, node->left->reg, node->right->reg, 8);
}

void compile_const_deref_write(struct AST *n)
{
	struct AST* node = n->right;
	store("mov", n->left->reg, node->right->imm*8, node->left->reg, "", 1);
}


void compile_not(struct AST *n)
{
	tmplabel(n->left->true_label);
	jmptmp("jmp", n->false_label);
	tmplabel(n->left->false_label);
}

void compile_notnot(struct AST *n)
{
	// no room for optimization
	tmplabel(n->left->left->true_label);
	jmptmp("jmp", n->left->false_label);
	tmplabel(n->left->left->false_label);
	tmplabel(n->left->true_label);
	jmptmp("jmp", n->false_label);
	tmplabel(n->left->false_label);
}

void compareimm_left(const char *mn, struct AST *n)
{
	printf("%s $%ld, %%%s\n", mn, n->left->imm, n->right->reg);
}
void compareimm_right(const char *mn, struct AST *n)
{
	printf("%s $%ld, %%%s\n", mn, n->right->imm, n->left->reg);
}

void comparereg(const char *mn, struct AST *n)
{
	printf("%s %%%s, %%%s\n", mn, n->right->reg, n->left->reg);
}

int nested_function_call = 0;

void compile_restore(struct AST *n)
{
	int i;
	const char *regs[REGALLOC_MAXREGS];
	int count = regalloc_locals(regallocator, regs);
	for(i = 0; i < count; i++){
		lealike("movq", -8*i-8, "rbp", "", 1, regs[i]);
	}
	count = regalloc_tmps(regallocator, regs);
	for(i = 0; i < count; i++){
		if(regs[i] != return_reg_name()){
			printf("pop %%%s\n", regs[i]);
		}else{
			const char *tmp = regalloc_tmp(regallocator);
			printf("pop %%%s\n #restore\n", tmp);
			printf("xchg %%%s, %%%s\n", tmp, return_reg_name());
			n->reg = tmp;
		}
	}

}

void compile_callersave()
{
	const char *regs[REGALLOC_MAXREGS];
	int i;
	if(nested_function_call == 0){ 
		int count = regalloc_locals(regallocator, regs);
		for(i = 0; i < count; i++){
			store("movq", regs[i], -8*i-8, "rbp", "", 1);
		}
	}
	int count = regalloc_tmps(regallocator, regs);
	for(i = 0; i < count; i++){
		printf("push %%%s # save tmp\n", regs[i]);
	}
	nested_function_call++;
}

void compile_push(struct AST *n)
{
	if(n->right->reg != NULL){
		printf("push %%%s\n", n->right->reg);
		regalloc_freeiftmp(regallocator,n->right->reg);
	}else{
		printf("pushq $%ld\n", n->right->imm);
	}

	if(n->left != NULL && n->left->type == ASTT_PUSH)
		n->pushedbytes = n->left->pushedbytes;

	n->pushedbytes += 8;
}

void compile_functioncall(struct AST *n)
{
	int i;
	for(i = 0; i < n->left->pushedbytes/8; i++){
		const char *dst = regalloc_paramreg(i);
		if(dst == NULL) break;
		printf("pop %%%s\n", dst);
	}

	// store correct number in node
	n->pushedbytes = n->left->pushedbytes - 8*i;

	printf("call %s\n", n->n);
	if(n->pushedbytes > 0)
		printf("add $%u, %%rsp\n", n->pushedbytes);
	
	n->reg = NULL;
	nested_function_call--;
	compile_restore(n);

	if(n->reg == NULL)
		n->reg = regalloc_return_reg(regallocator);
}
