#pragma once

#include <stdlib.h>
#include <stdint.h>

enum ASTT{
	ASTT_STATEMENT=0,
	ASTT_GOTO=1,
	ASTT_RETURN=2,
	ASTT_VARREAD=3,
	ASTT_VARWRITE=4,
	ASTT_PLUS=5,
	ASTT_MINUS=6,
	ASTT_MULT=7,
	ASTT_ARRAYREAD=8,
	ASTT_ARRAYWRITE=9,
	ASTT_INT=10,
	ASTT_LEA=11,
	ASTT_LABEL=12,
	ASTT_IF=13,
	ASTT_CAND=14,
	ASTT_CNOT=15,
	ASTT_CG=16,
	ASTT_CNEQ=17,
	ASTT_CALL=18,
	ASTT_PUSH=19,
	ASTT_CALLERSAVE=20,
	ASTT_NOCALLERSAVE=21
};

struct AST{
	enum ASTT type;
	struct AST *left, *right;
	const char *n;
	const char *reg;
	int pushedbytes, push_n;
	int64_t imm;
	struct burm_state *label;
	unsigned true_label, false_label;
};

struct AST* ast_statement(struct AST *labels, struct AST *stat, struct AST *before);
struct AST* ast_return(struct AST *left);
struct AST* ast_goto(const char *n);
struct AST* ast_plus(struct AST *left, struct AST *right);
struct AST* ast_mult(struct AST *left, struct AST *right);
struct AST* ast_minus(struct AST *left);
struct AST* ast_int(int64_t x);
struct AST* ast_varread(const char *s);
struct AST* ast_varwrite(const char *n, struct AST *left);
struct AST* ast_arraywrite(struct AST *left, struct AST *right);
struct AST* ast_arrayread(struct AST* left);
struct AST* ast_lea(struct AST* left, struct AST* right);
struct AST* ast_label(const char *s);
struct AST* ast_if(struct AST *left, const char *label, unsigned true_label, unsigned false_label);
struct AST* ast_cand(struct AST *left, struct AST *right, unsigned true_label, unsigned false_label);
struct AST* ast_cnot(struct AST *term, unsigned true_label, unsigned false_label, unsigned inv);
struct AST* ast_cg(struct AST *left, struct AST *right, unsigned true_label, unsigned false_label);
struct AST* ast_cneq(struct AST *left, struct AST *right, unsigned true_label, unsigned false_label);
struct AST* ast_call(struct AST *left, struct AST *right, const char *f);
struct AST* ast_push(struct AST *left, struct AST *right, int n);
struct AST* ast_callersave();
