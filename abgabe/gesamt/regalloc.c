#include "regalloc.h"

#include <stdio.h>
#include <assert.h>
#include <string.h>

#define INUSE_YES	'x'
#define INUSE_NO	' '

static const char *reg_all[] = { 
	"rdi", "rsi", "rdx",
	"rcx", "r8", "r9",
	"r10", "r11", "rax",
	};
static const char reg_return[] = "rax";

const char* return_reg_name()
{
#define RAX_INDEX 8
	assert(strcmp(reg_all[RAX_INDEX],"rax") == 0);
	return reg_all[RAX_INDEX];
}

const char* regalloc_return_reg(struct RegAllocator *r)
{
#define RAX_INDEX 8
	assert(strcmp(reg_all[RAX_INDEX],"rax") == 0);
	r->inuse[RAX_INDEX] = INUSE_YES;
	return reg_all[RAX_INDEX];
}

struct RegAllocator regalloc_new()
{
	struct RegAllocator r;
	r.p_count = 0;
	r.addvar = 0;
	regalloc_freealltmp(&r);
	return r;
}

const char* regalloc_paramreg(int n)
{
	assert(n >= 0);
	if(n > 5)
		return NULL;
	else
		return reg_all[n];
}

int regalloc_locals(const struct RegAllocator *const r, const char *regs[REGALLOC_MAXREGS])
{
	int i;
	for(i = 0; i < r->p_count+r->addvar; i++){
		regs[i] = reg_all[i];
	}
	return i;
}
int regalloc_tmps(const struct RegAllocator *const r, const char *regs[REGALLOC_MAXREGS])
{
	int i,j = 0;
	//fprintf(stderr,"tmps: %s\n",r->inuse);
	for(i = r->p_count+r->addvar; i < REGALLOC_MAXREGS; i++){
		if(r->inuse[i] == INUSE_YES){
			regs[j++] = reg_all[i];
		}
	}
	return j;
}

const char* regalloc_parameter(struct RegAllocator *r, const char *name)
{
	assert(r != NULL);
	assert(name != NULL);
	assert(r->addvar == 0);
	const int i = r->p_count++;
	r->a[i] = name;
	assert(r->inuse[i] == INUSE_NO);
	r->inuse[i] = INUSE_YES;
	return reg_all[i];
}
const char* regalloc_var(struct RegAllocator *r, const char *name)
{
	assert(r != NULL);
	assert(name != NULL);
	const int i = r->p_count+(r->addvar++);
	assert(i < REGALLOC_MAXREGS);
	r->a[i] = name;
	assert(r->inuse[i] == INUSE_NO);
	r->inuse[i] = INUSE_YES;
	return reg_all[i];
}
const char* regalloc_tmp(struct RegAllocator *r)
{
	assert(r != NULL);
	int i;
	for(i = REGALLOC_MAXREGS-1; i >= r->p_count+r->addvar; i--){
		if(r->inuse[i] == INUSE_NO){
			r->inuse[i] = INUSE_YES;
			return reg_all[i];
		}
	}
	assert(0);
	return NULL;
}
int regalloc_istmp(struct RegAllocator *r, const char *s)
{
	assert(r != NULL);
	assert(s != NULL);
	int i;
	for(i = r->p_count+r->addvar; i < REGALLOC_MAXREGS; i++){
		if(reg_all[i] == s){
			return 1;
		}
	}
	return 0;
}
void regalloc_freetmp(struct RegAllocator *r, const char *s)
{
	assert(r != NULL);
	assert(s != NULL);
	int i;
	for(i = r->p_count+r->addvar; i < REGALLOC_MAXREGS; i++){
		if(reg_all[i] == s){
			assert(r->inuse[i] == INUSE_YES);
			r->inuse[i] = INUSE_NO;
			return;
		}
	}
	assert(0);
}
void regalloc_freeiftmp(struct RegAllocator *r, const char *s)
{
	assert(r != NULL);
	assert(s != NULL);
	if(regalloc_istmp(r, s)){
		regalloc_freetmp(r, s);
	}
}

void regalloc_freealltmp(struct RegAllocator *r)
{
	assert(r != NULL);
	memset(r->inuse, INUSE_NO, REGALLOC_MAXREGS);
	r->inuse[REGALLOC_MAXREGS] = '\0';
}

const char* regalloc_reg(struct RegAllocator *r, const char *name)
{
	assert(r != NULL);
	assert(name != NULL);
	int i;
	const int all = r->p_count+r->addvar;
	for(i = 0; i < all; i++){
		if(strcmp(r->a[i], name) == 0){
			return reg_all[i];
		}
	}
	return NULL;
}

