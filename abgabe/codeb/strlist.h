#pragma once
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

enum SymbolOcc{ SYMBOL_ANY = 0, SYMBOL_USE = 1, SYMBOL_DEF = 2 };
enum SymbolType{ SYMBOL_LABEL = 1, SYMBOL_VARIABLE = 2 };

struct StrList{
	char *s;
	enum SymbolType typ;
	enum SymbolOcc occ;
	struct StrList *next;
};

struct StrList *newStrList(char *word, enum SymbolOcc occ, enum SymbolType typ, struct StrList *next);
struct StrList *concatStrList(struct StrList *x, struct StrList *y);
void printStrList(const struct StrList *x);
struct StrList* existsStrEx(struct StrList *x, const char *w, int occ, int typ);
struct StrList* existsStr(struct StrList *x, const char *w);
int checkStrList(struct StrList *x);

