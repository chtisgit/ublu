#include "ast.h"
#include <stdio.h>

static struct AST* ast_new(enum ASTT type, struct AST *left, struct AST *right, const char *n)
{
	struct AST *ast = malloc(sizeof(struct AST));
	ast->type = type;
	ast->left = left;
	ast->right = right;
	ast->reg = NULL;
	ast->n = n;
	ast->inv = 0;
	return ast;
}

struct AST* ast_statement(struct AST *labels, struct AST *stat, struct AST *before)
{
	if(labels == NULL && before == NULL)
		return stat;
	else if(labels == NULL)
		return ast_new(ASTT_STATEMENT, before, stat, NULL);
	else
		return ast_new(ASTT_STATEMENT, ast_statement(before, labels, NULL), stat, NULL);
}

struct AST* ast_return(struct AST *left)
{
	return ast_new(ASTT_RETURN, left, NULL, NULL);
}

struct AST* ast_goto(const char *n)
{
	return ast_new(ASTT_GOTO, NULL, NULL, n);
}

struct AST* ast_plus(struct AST *left, struct AST *right)
{
	return ast_new(ASTT_PLUS, left, right, NULL);
}
struct AST* ast_mult(struct AST *left, struct AST *right)
{
	return ast_new(ASTT_MULT, left, right, NULL);
}
struct AST* ast_minus(struct AST *left)
{
	return ast_new(ASTT_MINUS, left, NULL, NULL);
}

struct AST* ast_int(int64_t x)
{
	struct AST *ast = ast_new(ASTT_INT, NULL, NULL, NULL);
	ast->imm = x;
	return ast;
}

struct AST* ast_varwrite(const char *n, struct AST *left)
{
	return ast_new(ASTT_VARWRITE, left, NULL, n);
}

struct AST* ast_varread(const char *s)
{
	return ast_new(ASTT_VARREAD, NULL, NULL, s);
}

struct AST* ast_arraywrite(struct AST *left, struct AST *right)
{
	return ast_new(ASTT_ARRAYWRITE, left, right, NULL);
}

struct AST* ast_arrayread(struct AST* left)
{
	return ast_new(ASTT_ARRAYREAD, left, NULL, NULL);
}

struct AST* ast_lea(struct AST* left, struct AST* right)
{
	return ast_new(ASTT_LEA, left, right, NULL);
}

struct AST* ast_label(const char *s)
{
	return ast_new(ASTT_LABEL, NULL, NULL, s);
}

struct AST* ast_if(struct AST *left, const char *label, unsigned true_label, unsigned false_label)
{
	struct AST *ast = ast_new(ASTT_IF, left, NULL, label);
	ast->true_label = true_label;
	ast->false_label = false_label;
	return ast;
}

struct AST* ast_cand(struct AST *left, struct AST *right, unsigned true_label, unsigned false_label)
{
	struct AST *ast = ast_new(ASTT_CAND, left, right, NULL);
	ast->true_label = true_label;
	ast->false_label = false_label;
	return ast;
}
struct AST* ast_cnot(struct AST *term, unsigned true_label, unsigned false_label, unsigned inv)
{
	struct AST *ast = ast_new(ASTT_CNOT, term, NULL, NULL);
	ast->true_label = true_label;
	ast->false_label = false_label;
	ast->inv = inv;
	return ast;
}
struct AST* ast_cg(struct AST *left, struct AST *right, unsigned true_label, unsigned false_label)
{
	struct AST *ast = ast_new(ASTT_CG, left, right, NULL);
	ast->true_label = true_label;
	ast->false_label = false_label;
	return ast;
}
struct AST* ast_cneq(struct AST *left, struct AST *right, unsigned true_label, unsigned false_label)
{
	struct AST *ast = ast_new(ASTT_CNEQ, left, right, NULL);
	ast->true_label = true_label;
	ast->false_label = false_label;
	return ast;
}
