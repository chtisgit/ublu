%top{
#include "oxout.tab.h"

int exitcode = 0;

void remove_(char *s)
{
	int i, j = 0;
	for(i = 0; s[i] != '\0'; i++){
		if(s[i] != '_'){
			s[j++] = s[i];
		}
	}
	s[j] = '\0';
}

}

%x comment

ONE	";"|"("|")"|","|":"|"="|">"|"["|"]"|"-"|"+"|"*"
TWO	"!="
ID	[A-Za-z][A-Za-z0-9]*
NUM	[0-9_]+
COMM	"(""*"[^*]*"*"")"

%%

<INITIAL>"(""*"		BEGIN(comment);
<comment>"*"")"		BEGIN(INITIAL);
<comment>[^*]*		/* eat */
<comment>"*"[^*)]*	/* eat */

#{COMM}	;
{ONE}	return yytext[0];
"!="	return OP_NE;
if	return KW_IF;
and	return KW_AND;
end	return KW_END;
not	return KW_NOT;
var	return KW_VAR;
goto	return KW_GOTO;
return	return KW_RETURN;

{ID}	return ID;	@{
				@ID.str@ = strdup(yytext);
			@}
{NUM}	remove_(yytext); return NUM; @{
		@NUM.value@ = atol(yytext); 
	@}

[ \t\n]	/* eat */
.	{
		fprintf(stderr, "error\n");
		exit(1);
	}


%%

int yywrap()
{
	return 1;
}
