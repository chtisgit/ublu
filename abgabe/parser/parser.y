%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define YYSTYPE char*

void yyerror(const char *s);
extern char *yytext;
extern FILE *yyin;

%}

%token KW_IF
%token KW_AND
%token KW_END
%token KW_NOT
%token KW_VAR
%token KW_GOTO
%token KW_RETURN
%token OP_NE
%token ID 
%token NUM


%%

Program:	
		| Funcdefs
		;
Funcdefs:	Funcdef ';'
		| Funcdefs Funcdef ';'
		;
Funcdef:	id '(' Pars ')' Stats KW_END /* Funktionsdefinition */
		;
ParList:	ParList ',' id
		| ParList ',' 
		| id
		;
Pars:		
		| ParList /* Parameterdefinition */
		;
Stats:		| NEStats;
NEStats:	Labeldefs Stat ';'
		| NEStats Labeldefs Stat ';'
		;
Labeldefs:
		| Labeldefs Labeldef;
Labeldef:	id ':' /* Labeldefinition */
		;
Stat:		KW_RETURN Expr
		| KW_GOTO id
		| KW_IF Cond KW_GOTO id
		| KW_VAR id '=' Expr /* Variablendefinition */
		| Lexpr '=' Expr /* Zuweisung */
		| Term
		;
Cond:		CondAnd
		| KW_NOT Cterm
		;
CondAnd:	Cterm
		| CondAnd KW_AND Cterm;
Cterm:		'(' Cond ')'
		| Expr OP_NE Expr
		| Expr '>' Expr
		;
Lexpr:		id /* schreibender Variablenzugriff */
		| Term '[' Expr ']' /* schreibender Arrayzugriff */
		;
Expr:		PlusTerms
		| MulTerms
		| PreMinusTerm 
		;
PlusTerms:	Term '+' Term
		| PlusTerms '+' Term
		;
MulTerms:	Term '*' Term
		| MulTerms '*' Term
		;
PreMinusTerm:	Term
		| '-' PreMinusTerm 
		;
ExprList:	
		| NEExprList
		;
NEExprList:	NEExprList ',' Expr
		| NEExprList ',' 
		| Expr 
		;
Term:		'(' Expr ')'
		| NUM
		| Term '[' Expr ']' /* lesender Arrayzugriff */
		| id /* lesender Variablenzugriff */
		| id '(' ExprList ')' /* Funktionsaufruf */
		;
id:		ID { $$ = strdup(yytext); }

%%

extern int exitcode;

void yyerror(const char *s)
{
	fprintf(stderr, "error: %s\n", s);
	exit(2);
}

int main()
{
	yyin = stdin;
	yyparse();
	return exitcode;
}

