%top{
#include "parser.tab.h"

int exitcode = 0;

}

%x comment

ONE	";"|"("|")"|","|":"|"="|">"|"["|"]"|"-"|"+"|"*"
TWO	"!="
ID	[A-Za-z][A-Za-z0-9]*
NUM	[0-9_]+
COMM	"(""*"[^*]*"*"")"

%%

<INITIAL>"(""*"		BEGIN(comment);
<comment>"*"")"		BEGIN(INITIAL);
<comment>[^*]*		/* eat */
<comment>"*"[^*)]*	/* eat */

#{COMM}	;
{ONE}	return yytext[0];
"!="	return OP_NE;
if	return KW_IF;
and	return KW_AND;
end	return KW_END;
not	return KW_NOT;
var	return KW_VAR;
goto	return KW_GOTO;
return	return KW_RETURN;

{ID}	return ID;
{NUM}	{
		const char *s = yytext;
		while(*s == '0' || *s == '_'){
			s++;
		}
		if(*s == '\0'){
			s = "0";
		}
		printf("num ");
		while(*s != '\0'){
			if(*s != '_'){
				printf("%c", *s);
			}
			s++;
		}
		printf("\n");
		return NUM;
	}
[ \t\n]	/* eat */
.	{
		printf("error\n");
		exitcode = 1;
	}


%%

int yywrap()
{
	return 1;
}
