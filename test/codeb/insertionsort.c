
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
extern long sort(long*, long);

static long *A, N;

int setup()
{
	int i;
	A = malloc(sizeof(long)*1024);
	N = 1024;
	for(i = 0; i < N; i++)
		A[i] = rand();
	return 1;
}

int sorted_test()
{
	int i;
	for(i = 0; i < N-1; i++){
		if(A[i] > A[i+1]){
			int j = (i > 20) ? i-20 : 0;
			int m = (i >= N-20) ? N-1 : i+18;
			printf("error after sort:\n");
			for(; j <= m; j++){
				if(j == i || j == i+1)
					printf("A[%ld] = %ld <--\n", j, A[j]);
				else
					printf("A[%ld] = %ld\n", j, A[j]);
			}
			return 0;
		}
	}
	return 1;
}


long sort(long *a, long n)
{
	long i = 0;
	
	if (!(n > 0)) goto X;
	n = n + (-1);

/* outer loop */
A:	if (!(a[i] > a[i+1])) goto noB;

/* inner loop find correct place for a[i+1] */
	long j = i;
	goto yesB;
B:	j = j + (-1);
yesB:	if (a[j] > a[i+1] && j != 0) goto B;
/* correct location is in j (j >= 0) */

	long val = a[i+1];
	long k = i+1;
	goto yesC;

C:	k = k + (-1);
yesC:	a[k+1] = a[k];
	if(k != j) goto C;
	a[j] = val;


noB:	i = i + 1;
	if(i != n) goto A;

X:	return 1;
}

int main()
{
	setup();
	printf("A[0] = %ld\nA[1] = %ld\nA[2] = %ld\n", A[0], A[1], A[2]);
	sort(A,N);
	sorted_test();
	return 0;
}
