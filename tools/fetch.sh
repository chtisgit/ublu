#!/bin/sh

sumincorrect(){
	echo "SHA256 sums not correct"
	exit 1
}

rm -f oxDistG1.04.sh ox.patch iburg.tar.gz
rm -Rf iburg ox

curl http://www.complang.tuwien.ac.at/ublu/tools/ox/oxDistG1.04.sh.Z | zcat > oxDistG1.04.sh
curl -o ox.patch http://www.complang.tuwien.ac.at/ublu/tools/ox/ox-flex-new-gcc4.patch
curl -o iburg.tar.gz http://www.complang.tuwien.ac.at/ublu/tools/iburg09.tar.gz

# check sha
sha256sum -c SHA256 || sumincorrect

# unpack ox and iburg
sh oxDistG1.04.sh
tar xzf iburg.tar.gz
mv oxDistG1.04 ox
mv iburg-bison iburg

# patch ox
cd ox
patch -p1 < ../ox.patch
cd ..

# delete archives
rm -f oxDistG1.04.sh ox.patch iburg.tar.gz
