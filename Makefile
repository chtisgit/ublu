MAKE ?= make

all: tools/bin/iburg tools/bin/bfe tools/bin/ox  abgabe/asma/asma abgabe/asmb/asmb abgabe/scanner/scanner abgabe/parser/parser abgabe/ag/ag abgabe/codea/codea abgabe/codeb/codeb abgabe/gesamt/gesamt

tools/bin/iburg tools/bin/bfe tools/bin/ox:
	cd tools && $(MAKE)

abgabe/asma/asma abgabe/asmb/asmb abgabe/scanner/scanner abgabe/parser/parser abgabe/ag/ag abgabe/codea/codea abgabe/codeb/codeb abgabe/gesamt/gesamt:
	cd abgabe && $(MAKE)

clean:
	cd tools && $(MAKE) clean
	cd abgabe && $(MAKE) clean

.PHONY: all clean tools abgabe
